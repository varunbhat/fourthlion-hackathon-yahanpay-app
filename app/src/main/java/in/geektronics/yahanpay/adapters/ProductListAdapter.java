package in.geektronics.yahanpay.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.geektronics.yahanpay.R;
import in.geektronics.yahanpay.models.Product;
import in.geektronics.yahanpay.models.ProductOrder;
import in.geektronics.yahanpay.models.User;

public class ProductListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Product>> _listDataChild;

    private User user;


    public ProductListAdapter(Context context, List<String> listDataHeader,
                              HashMap<String, List<Product>> listChildData) {
        this.mContext = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Product getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    public void setUser(User user){
        this.user = user;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = getChild(groupPosition, childPosition).getEnglishName();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_product, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.product_title);
        txtListChild.setText(childText);

        convertView.findViewById(R.id.decrease_count).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView lCountView = ((TextView) (((View) (v.getParent())).findViewById(R.id.product_count)));
                int count;

                try {
                    count = new Integer(lCountView.getText().toString());
                } catch (NumberFormatException e) {
                    count = 0;
                }

                if (count > 0) {
                    lCountView.setText("" + (--count));
                }
                getChild(groupPosition,childPosition).setProductCount(count);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView lCountView = ((TextView) v.findViewById(R.id.product_count));

                lCountView.setVisibility(View.VISIBLE);
                ((TextView) v.findViewById(R.id.decrease_count)).setVisibility(View.VISIBLE);

                int count = new Integer(lCountView.getText().toString())+1;
                lCountView.setText("" + count);
                getChild(groupPosition,childPosition).setProductCount(count);
            }
        });

        return convertView;
    }

    public List<ProductOrder> getProductOrder(){
        List<ProductOrder> lListProductOrder = new ArrayList<>();

        for (int i=0;i<_listDataChild.size();i++){
            for (Product lProd : this._listDataChild.get(this._listDataHeader.get(i))){
                if (lProd.getProductCount() > 0) {
                    lListProductOrder.add(new ProductOrder(this.user, lProd, lProd.getProductCount()));
                }
            }
        }
        return lListProductOrder;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public String getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_category, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.category_title);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}