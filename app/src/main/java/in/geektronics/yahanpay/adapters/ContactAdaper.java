package in.geektronics.yahanpay.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.geektronics.yahanpay.R;
import in.geektronics.yahanpay.models.User;

/**
 * Created by varunbhat on 22/03/15.
 */
public class ContactAdaper extends ArrayAdapter<User> {
    private ArrayList<User> memberList;
    private Context mContext;


    public ContactAdaper(Context context, ArrayList<User> objects) {
        super(context, R.layout.contact_row, objects);
        this.mContext = context;
        this.memberList = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(mContext);

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.contact_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.memberName.setText(memberList.get(position).getName());
        holder.memberDetails.setText(memberList.get(position).getMobile());
        holder.memberContactSelected.setChecked(memberList.get(position).isSelected());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.memberContactSelected.setChecked(!holder.memberContactSelected.isChecked());
                memberList.get(position).setSelected(holder.memberContactSelected.isChecked());
            }
        });

        holder.memberContactSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                memberList.get(position).setSelected(isChecked);
            }
        });
        return convertView;
    }

    public ArrayList<User> getContactList() {
        ArrayList<User> lRetVal = new ArrayList<User>();


        for (User contact : memberList) {
            if (contact.isSelected()) {
                lRetVal.add(contact);
                Log.d("debug", contact.getName());
            }
        }

        return lRetVal;
    }

    static class ViewHolder {
        @InjectView(R.id.textview_member_name)
        TextView memberName;
        @InjectView(R.id.textview_member_number)
        TextView memberDetails;
        @InjectView(R.id.contact_image)
        ImageView memberContactImage;
        @InjectView(R.id.select_contact)
        CheckBox memberContactSelected;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
