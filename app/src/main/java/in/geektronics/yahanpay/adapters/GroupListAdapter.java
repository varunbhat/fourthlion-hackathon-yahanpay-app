package in.geektronics.yahanpay.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

import in.geektronics.yahanpay.R;
import in.geektronics.yahanpay.models.GrahakGroup;

/**
 * Created by varunbhat on 22/03/15.
 */
public class GroupListAdapter extends ArrayAdapter<GrahakGroup> {
    private ArrayList<GrahakGroup> memberList;
    private Context mContext;


    public GroupListAdapter(Context context, ArrayList<GrahakGroup> objects) {
        super(context, R.layout.item_card_overview, objects);
        this.mContext = context;
        this.memberList = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(mContext);

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.item_card_overview, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.groupCardName.setText(memberList.get(position).getGroupName());
//        holder.groupCardItemCount.setText(Select.from(GroupRegistrations.class).where(
//                Condition.prop("grahak_group").eq(memberList.get(position).getId())).list().size());

        return convertView;
    }


    static class ViewHolder {
        @InjectView(R.id.card_text)
        TextView groupCardName;
        @InjectView(R.id.card_item_count)
        TextView groupCardItemCount;
        @InjectView(R.id.card_image)
        ImageView groupListImage;



        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
