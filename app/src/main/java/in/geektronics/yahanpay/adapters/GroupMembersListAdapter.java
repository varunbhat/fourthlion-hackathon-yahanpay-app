package in.geektronics.yahanpay.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.geektronics.yahanpay.AdhaarDisplayActivity;
import in.geektronics.yahanpay.R;
import in.geektronics.yahanpay.models.User;

/**
 * Created by varunbhat on 12/04/15.
 */


public class GroupMembersListAdapter extends ArrayAdapter<User> {
    private ArrayList<User> memberList;
    private Context mContext;



    public GroupMembersListAdapter(Context context, ArrayList<User> objects) {
        super(context, R.layout.list_membership_row, objects);
        this.mContext = context;
        this.memberList = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(mContext);

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.list_membership_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.memberName.setText(memberList.get(position).getName());
        holder.memberDetails.setText(memberList.get(position).getMobile());


        if (memberList.get(position).getIsUserVerified()) {
            holder.memberContactStatus.setText("Verified");
            holder.memberContactStatus.setTextColor(Color.parseColor("#297a4a"));
//            ((ShapeDrawable)holder.memberContactStatus.getBackground()).getPaint().setColor(Color.parseColor("#297a4a"));
        }else {
            holder.memberContactStatus.setText("Not Verified");
            holder.memberContactStatus.setTextColor(Color.parseColor("#aa3939"));
        }

        holder.memberContactImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AdhaarDisplayActivity.class);
                intent.putExtra("USER_ID", memberList.get(position).getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        @InjectView(R.id.textview_member_name)
        TextView memberName;

        @InjectView(R.id.textview_member_number)
        TextView memberDetails;

        @InjectView(R.id.contact_image)
        ImageView memberContactImage;

        @InjectView(R.id.contact_status)
        TextView memberContactStatus;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
