package in.geektronics.yahanpay.utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ABHIJEET on 07-01-2015.
 */
public class CSVFileParser {

    public CSVFileParser(){
    }

    private static List<String[]> read(InputStream inputStream){
        List<String[]> resultList = new ArrayList<String[]>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String csvLine;
            while ((csvLine = reader.readLine()) != null) {
                String[] row = csvLine.split(",");
                resultList.add(row);
            }
        }
        catch (IOException ex) {
            throw new RuntimeException("Error in reading CSV file: "+ex);
        }
        finally {
            try {
                if(inputStream!=null)
                inputStream.close();
            }
            catch (IOException e) {
                throw new RuntimeException("Error while closing input stream: "+e);
            }
        }
        return resultList;
    }


    public static List<String[]> parseFileFromInputStream(InputStream in){
        return read(in);
    }

    public static  List<String[]> parseFileFromURL(String url) throws MalformedURLException {
        try {
            InputStream in = new URL(url).openConnection().getInputStream();
            return read(in);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<String[]> parseFileFromResource(Context context,int resource_id){

            InputStream in = context.getResources().openRawResource(resource_id);
            return read(in);
    }


}