package in.geektronics.yahanpay.fragments.group_list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.geektronics.yahanpay.ListMembersActivity;
import in.geektronics.yahanpay.R;
import in.geektronics.yahanpay.adapters.GroupListAdapter;
import in.geektronics.yahanpay.models.GrahakGroup;


public class GroupListViewFragment extends Fragment {
    @InjectView(R.id.group_list_view)
    ListView mGroupListView;

    GroupListAdapter mGroupListAdapter;

    private OnFragmentInteractionListener mListener;
    List<GrahakGroup> mGroups;

    public GroupListViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_group_list, container, false);
        ButterKnife.inject(this,view);

        mGroups = GrahakGroup.find(GrahakGroup.class, "is_admin=?", "1");

        mGroupListAdapter = new GroupListAdapter(getActivity().getApplicationContext(), new ArrayList<>(mGroups));

        mGroupListView.setAdapter(mGroupListAdapter);

        mGroupListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putLong("GROUP_ID",mGroups.get(position).getId());

                Intent intent = new Intent(getActivity().getApplicationContext(), ListMembersActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String id);
    }
}
