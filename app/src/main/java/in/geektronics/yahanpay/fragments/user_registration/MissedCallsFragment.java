package in.geektronics.yahanpay.fragments.user_registration;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import in.geektronics.yahanpay.ListMembersActivity;
import in.geektronics.yahanpay.R;
import in.geektronics.yahanpay.adapters.ContactAdaper;
import in.geektronics.yahanpay.models.GrahakGroup;
import in.geektronics.yahanpay.models.GroupRegistrations;
import in.geektronics.yahanpay.models.User;
import in.geektronics.yahanpay.network.NetUtils;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class MissedCallsFragment extends Fragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private ContactAdaper adapter;

    private ArrayList<User> userList;

    long mGroupId = -1;

    @InjectView(R.id.contact_list)
    ListView contactsList;

    @OnClick(R.id.add_group)
    public void addGroup() {
        GrahakGroup lGrahakGroup;
        Log.d("debug", "" + adapter.getContactList().size());
        Log.d("debug", "Group ID check:" + mGroupId);
        mGroupId = -1;

        if (mGroupId == -1) {
            String lGroupId = "1";

            if (!lGroupId.matches("^ *[\\d]+ *$")) {
                return;
            } else {
                List<GrahakGroup> lGrahakGroupRes = GrahakGroup.find(GrahakGroup.class, "group_id=?", lGroupId);
                if (lGrahakGroupRes.size() == 0) {
                    return;
                } else {
                    lGrahakGroup = lGrahakGroupRes.get(0);
                }
            }
        } else {
            Log.d("debug", "Called from activity with Group ID:" + mGroupId);
            List<GrahakGroup> lGrahakGroupRes = GrahakGroup.find(GrahakGroup.class, "group_id=?", ""+ mGroupId);
            if (lGrahakGroupRes.size() == 0) {
                Log.e("debug", "Called from activity with Group ID:" + mGroupId);
//                mGroupNameView.setError("No Such Group Exists");
                return;
            } else {
                lGrahakGroup = lGrahakGroupRes.get(0);
            }
        }

        ArrayList<String> lMobileConfirmationNumbers = new ArrayList<>();


        // Get All Selected Contacts
        ArrayList<User> lSelectedContactList = adapter.getContactList();

        if(lSelectedContactList.size() == 0){
            Toast.makeText(getActivity().getApplicationContext(), "No Contact Selected", Toast.LENGTH_LONG).show();
            return;
        }

        for ( User lContact : lSelectedContactList){
            Log.d("CreateGroup DB Update", "Name:" + lContact.getName() + " Contact:" + lContact.getMobile().replaceAll("[^\\d+]", ""));

            List<User> data = User.find(User.class, "mobile=?", lContact.getMobile().replaceAll("[^\\d+]",""));

            Log.d("CreateGroupDB", "FoundSize:" + data.size());
            User changeContact;

            if (data.size() > 0){
                changeContact = data.get(0);
                changeContact.setName(lContact.getName());
                changeContact.save();
            }
            else {
                changeContact = new User(lContact.getName(),lContact.getMobile().replaceAll("[^\\d+]",""));
                changeContact.save();
                lMobileConfirmationNumbers.add(lContact.getMobile());
            }

            if(GroupRegistrations.find(GroupRegistrations.class,
                    "contact=? and grahak_group=?",
                    changeContact.getId().toString(),
                    lGrahakGroup.getId().toString()).size()==0) {
                new GroupRegistrations(changeContact, lGrahakGroup).save();
            }
            else {
                Log.d("CreateGroupDB", "Group:" + lGrahakGroup.getGroupName() + ", Contact " + changeContact.getMobile() + "already Present");
            }
        }

        lGrahakGroup.setIsAdmin(true);
        lGrahakGroup.save();

        if (!NetUtils.isOnline(getActivity().getApplicationContext())) {
//            // Todo: Update Online! (Or use sync service)
//            Log.d("CreateGroupDB","Sending sms");
//            for (String lMobNum : lMobileConfirmationNumbers) {
//                Log.d("debug", "Sending Sms " + lMobNum);
//
//                if (lMobNum.length() >= 10) {
//                    SmsManager smsManager = SmsManager.getDefault();
//                    smsManager.sendTextMessage(lMobNum, null, "Hello, I have added you to the Mumbai Grahak Panchayat Group." +
//                            "To confirm your addition, Please reply with YES.", null, null);
//                }
//            }
        } else {
            Log.d("CreateGroupDB", "Internet is online");
        }

        Log.d("CreateGroupDB", "Internet is online");
        Intent intent = new Intent(getActivity().getApplicationContext(), ListMembersActivity.class);
        startActivity(intent);

    }

    @InjectView(R.id.contact_retrival_progress)
    RelativeLayout contactProgress;

    public MissedCallsFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MissedCallsFragment newInstance(int sectionNumber) {
        MissedCallsFragment fragment = new MissedCallsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ArrayList<User> getRecentContacts() {
        boolean isFirst = true;
        boolean querySuccess = false;
        int queryCount;
        ArrayList<User> userList = new ArrayList<>();

        Cursor c = getActivity().getApplicationContext().getContentResolver().query(android.provider.CallLog.Calls.CONTENT_URI,null, null, null, CallLog.Calls.DATE + " DESC");

        queryCount = c.getCount();

        while (true) {
            if (queryCount == 0) {
                c.close();
                break;
            } else if (isFirst) {
                querySuccess = c.moveToFirst();
                isFirst = false;
            } else if (!isFirst) {
                querySuccess = c.moveToNext();
                if (c.isAfterLast()) {
                    c.close();
                    break;
                }
            }

            if (querySuccess) {
                String name = c.getString(c.getColumnIndex(CallLog.Calls.CACHED_NAME));
                String phoneNumber = "";
                Bitmap image;

//                if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    phoneNumber = c.getString(c.getColumnIndex(CallLog.Calls.NUMBER));
                    //Log.d(this.getClass().getName(), "name:" + name + " PhoneNumber:" + phoneNumber);
//                }
                userList.add(new User(name, phoneNumber));
            }
        }
        return userList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_contact_list, container, false);
        ButterKnife.inject(this, view);

        userList = getRecentContacts();
        adapter = new ContactAdaper(getActivity().getApplicationContext(), userList);
        contactsList.setAdapter(adapter);

        contactProgress.setVisibility(View.GONE);
        contactsList.setVisibility(View.VISIBLE);

        return view;
    }
}
