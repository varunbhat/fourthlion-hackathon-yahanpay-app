package in.geektronics.yahanpay;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import in.geektronics.yahanpay.models.AadhaarCard;

public class OfflinePlotting extends FragmentActivity {

	
	GoogleMap googleMap;
	double latSum=0, lngSum=0;
    public String TAG = "Offline Plotting" ;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);
		
        try {
            // Loading map
            initilizeMap();
    		if(this.setMarkers()){
                CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latSum,lngSum)).tilt(45).zoom(9).build();
    		    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    		}
    		else{
    			CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(23.25, 77.41)).tilt(45).zoom(8).build();
    		    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    			Toast.makeText(getApplicationContext(),
                        "Sorry! No Places to plot !", Toast.LENGTH_SHORT)
                        .show();
    		}
            googleMap.setMyLocationEnabled(true);
            
            
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Exception");
        }
	}
	
    private boolean setMarkers() {
		// TODO Auto-generated method stub
        ArrayList<AadhaarCard> cards = new ArrayList<>(AadhaarCard.find(AadhaarCard.class, null, null));
    	Log.d(TAG, "" + cards.size());
    	boolean foundPlaces = false;
    	int count =0;
    	for(AadhaarCard card : cards){
    		Log.d("Crad", card.toString());
    		if(card.getLatlng() != null){
    			LatLng ll = card.getLatlng();
    			if(ll.latitude == 0 || ll.longitude==0){	
    			}
    			else{
    				googleMap.addMarker(new MarkerOptions().position(ll).title(card.name));
    				Log.d(TAG, "Marker added : " + card.name);
        			latSum +=ll.latitude;
        			lngSum +=ll.longitude;
        			count++ ;   				
    			}
    			foundPlaces = true ;
    		}
    	}
    	latSum /= count ;
    	lngSum /= count ;
		return foundPlaces;
	}

	//method to create map
    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(
                    R.id.map)).getMap();
 
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
}
