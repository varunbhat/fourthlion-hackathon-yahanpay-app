package in.geektronics.yahanpay.network;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import in.geektronics.yahanpay.models.AadhaarCard;

public class WebService extends AsyncTask<AadhaarCard, Void, AadhaarCard> {
	
	WebServiceListener listener = null;
	ProgressDialog dialog;
	LatLng latLng = null;
	AadhaarCard aadhaarCard ;
	Context context;
	public String TAG = "WEB_SERVICE";
	int index = 0;
	String API_KEY = "AIzaSyDHAGs_KZ0ClWuuZjwclXoC5p9c7zvQytk" ;
	String urlPlaces;
	String urlGeocode = "https://maps.googleapis.com/maps/api/geocode/json?address=";
	String components = "&components=country:IN|postal_code:" ;
	
	public WebService(Context c, WebServiceListener listener){
		context = c;
		this.listener  =  listener;
	}
	
	@SuppressLint("DefaultLocale")
	public String toSearch(AadhaarCard card){
		String location ;
// Select.from(PinCodeLocationMap.class).where(Condition.prop("pincode").eq(card.pincode)).first().getLocation();
		String vtc = card.vtc ;
		vtc = vtc.toLowerCase();
        location = "MahalahshmiLayout";
		if(location != null){
			//Toast.makeText(context, "Database gave : "+location, Toast.LENGTH_LONG).show();
			location = location.toLowerCase();
			String[] locArray = location.split(" ");
			for(int i=0;i<locArray.length;i++){
				if(vtc.contains(locArray[i])){
					String[] temp = vtc.split(locArray[i], 2);
					vtc = temp[0]+temp[1];
				}
			}
//			try{
//			String vtcTemp = vtc.trim();
//			//Remove double spaces
//			int i= vtcTemp.indexOf(" ",0);
//			while(i!= -1){
//				if(vtcTemp.charAt(i+1)==' '){
//					vtcTemp = vtcTemp.substring(0, i)+vtcTemp.substring(i+2);
//				}
//				i = vtcTemp.indexOf(" ", i+1);
//			}
//			}catch(Exception e){
//				Log.d("WebService toSearch", "Exception");
//				return vtc;
//			}
			
		}
		return vtc.trim().replace(" ", "+");
	}
	
    protected void onPreExecute(){
    	dialog= new ProgressDialog(context);
    	
    	dialog.setMessage("Loading Internet data");
    	dialog.show();
    }
	@Override
	protected void onPostExecute(AadhaarCard card) {
		if(dialog.isShowing())
			dialog.dismiss();
	    listener.onNetworkActionComplete(card);
	}
	String getAddress(AadhaarCard card){
		String url = null;
		Log.d(TAG, "call to getAddress");
		if(index == 0){
			url = urlGeocode+toSearch(card)+components +card.pincode+"&key="+API_KEY ;
		}
		else{
			if(index == 1){
			url = urlGeocode +card.lm.replace(" ", "+")+components+card.pincode+
					"&key="+API_KEY ;
			}
			else{
				url = urlGeocode +card.subdist.replace(" ", "+")+components+card.pincode+
						"&key="+API_KEY ;
			}
			
		}
		Log.d(TAG, url + "\nindex : " + index);index++;
		return url;
		
	}

    public WebService() {
        super();
    }

    @Override
	protected AadhaarCard doInBackground(AadhaarCard...params ) {
		Log.d(TAG, "In Webservice do_in_background");
		aadhaarCard = params[0];
    	Log.d(TAG, "received card \n" + aadhaarCard.name + " " + aadhaarCard.lat + " " + aadhaarCard.lng);
		try{ 
			Log.d(TAG, "started network activities");
			String URLgeo = this.getAddress(aadhaarCard);
			Log.d(TAG, "getAddress returned " + URLgeo);
//			String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query="+
//    			address+"&key="+ API_KEY;
			URL urlObj = new URL(URLgeo);
			Log.d(TAG, "requesting URL  : " + URLgeo);
			HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
		
			Log.d(TAG, "connection success");
			StringBuffer response = new StringBuffer();
			response.append(in.readLine());
			while ((inputLine = in.readLine()) != null) {
				response.append("\n"+inputLine);
				Log.d(TAG + " Response", ":" + inputLine + ":\n");
			}
			in.close();
			//Log.d("JSON", response.toString());
			JSONObject jsonResponse = new JSONObject(response.toString());
			String status = jsonResponse.getString("status");
			Log.d(TAG, "Status : " + status);
			
			if(status.equalsIgnoreCase("OK")){
				String results = "results" ;
				String location = "location", lat = "lat",lng="lng";
				JSONObject bestMatch = jsonResponse.getJSONArray(results).getJSONObject(0);
				JSONObject geometry = bestMatch.getJSONObject("geometry");
				//Log.d("geometry", geometry.toString());
				aadhaarCard.lat = String.valueOf(geometry.getJSONObject(location).getDouble(lat));
				aadhaarCard.lng = String.valueOf(geometry.getJSONObject(location).getDouble(lng));
			}
			
			String zeroRes = "ZERO_RESULTS" ;
			if(status.equalsIgnoreCase(zeroRes)){
				aadhaarCard = this.doInBackground(aadhaarCard);
			}

			if(!aadhaarCard.lat.equals("0") || !aadhaarCard.lng.equals("0")){
				Log.d(TAG + " LATLNG : ", aadhaarCard.lat + " " + aadhaarCard.lng);
				return aadhaarCard;
			}
		}catch(Exception e){
			Log.d(TAG + " do_in_background", e.getMessage());
		}
		

		return null;
	}
}

