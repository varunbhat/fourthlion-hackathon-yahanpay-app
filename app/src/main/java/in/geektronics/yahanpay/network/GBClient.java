package in.geektronics.yahanpay.network;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;

import in.geektronics.yahanpay.models.AadhaarCard;

/**
 * Created by ABHIJEET on 22-01-2015.
 */
public class GBClient {
    //        private static final String BASE_URL = "http://yahanpay.geektronics.in";
    private static final String BASE_URL = "http://tests.dialme.yahanpay.in:8080";

    private static AsyncHttpClient client = new AsyncHttpClient();

//    public static void createGroup(String uid,ProductGroup group,JsonHttpResponseHandler responseHandler){
//
//        RequestParams params = new RequestParams();
//        params.put(ACTION_TAG,ACTION_CREATE);
//        params.put(PROPERTY_USER_ID,uid);
//        params.put(PROPERTY_GROUP_NAME,group.getGroupName());
//        params.put(PROPERTY_GROUP_TITLE,group.getGroupTitle());
//
//        post(ENDPOINT_GROUP,params,responseHandler);
//
//    }

    public static void uploadAdhaarCardArray(Context context, ArrayList<AadhaarCard> adhaarArray, JsonHttpResponseHandler responseHandler) {
        JSONObject jo = new JSONObject();
        Collection<JSONObject> items = new ArrayList<>();
        StringEntity lSendData = null;

        for (AadhaarCard card : adhaarArray) {
            items.add(card.toJsonObject());
        }

        try {
            JSONArray lJsonArray = new JSONArray(items);
            jo.put("data", lJsonArray);
            Log.d("GBCLIENT", "Json Data: " + jo.toString());
            lSendData = new StringEntity(jo.toString(), HTTP.UTF_8);
        } catch (JSONException e){
        } catch (UnsupportedEncodingException e){
        }



//        DefaultHttpClient httpClientStatic = new DefaultHttpClient();
//
//        HttpGet httpget = new HttpGet(BASE_URL);
//        HttpResponse httpResponse = null;
//
//        try {
////            httpResponse = httpClientStatic.execute(httpget);
//        } catch (ClientProtocolException e) {
//        } catch (IOException e) {
//        }
//
//        HttpEntity httpEntity = httpResponse.getEntity();
        postArray(context, "/dashboard/adhaar", lSendData, responseHandler);
    }

    private static void postArray(Context context, String url, HttpEntity entity, JsonHttpResponseHandler responseHandler) {
        client.post(context, getAbsoluteUrl(url), entity, "application/json", responseHandler);
    }

    private static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.d("Post Request", "Address:" + getAbsoluteUrl(url));
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
