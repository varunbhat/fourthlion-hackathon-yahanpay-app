package in.geektronics.yahanpay.network;

import in.geektronics.yahanpay.models.AadhaarCard;

public interface WebServiceListener {

	public void onConnectionCheck(boolean b); 
    public void onNetworkActionComplete(AadhaarCard card);
}