package in.geektronics.yahanpay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import in.geektronics.yahanpay.adapters.GroupMembersListAdapter;
import in.geektronics.yahanpay.models.AadhaarCard;
import in.geektronics.yahanpay.models.GroupRegistrations;
import in.geektronics.yahanpay.models.User;
import in.geektronics.yahanpay.network.GBClient;
import in.geektronics.yahanpay.network.NetUtils;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class ListMembersActivity extends ActionBarActivity {
    Long mGroupId = -1L;

    ArrayList<User> mGroupUsers;
    GroupMembersListAdapter mGroupMembersListAdapter;

    @InjectView(R.id.user_list)
    ListView mUserListView;

    @OnClick(R.id.submit_button)
    void finalSubmit(){
        if (NetUtils.isOnline(this)){
            GBClient.uploadAdhaarCardArray(getApplicationContext(),new ArrayList<AadhaarCard>(AadhaarCard.find(AadhaarCard.class,null,null)),
                    new JsonHttpResponseHandler()
                    {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable){
                            super.onFailure(statusCode, headers, responseString, throwable);
                            Toast.makeText(getApplicationContext(),responseString,Toast.LENGTH_LONG).show();
                        }
                    });
        } else {
            Toast.makeText(this, "Not Connected to internet", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.view_report)
    void viewReport(){
        Intent intent = new Intent(getApplicationContext(),ProductReportActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.default_font))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        setContentView(R.layout.activity_list_members);
        ButterKnife.inject(this);

//        final Intent intent = getIntent();
//        final Bundle data = intent.getExtras();
        mGroupUsers = new ArrayList<>();
//
//        mGroupId = data.getLong("GROUP_ID");
        mGroupId = 1L;

        updateUserList();

        mGroupMembersListAdapter = new GroupMembersListAdapter(getApplicationContext(), mGroupUsers);
        mUserListView.setAdapter(mGroupMembersListAdapter);

        View footerView = getLayoutInflater().inflate(R.layout.list_members_footer, mUserListView, false);
        mUserListView.addFooterView(footerView);

        mUserListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Debug", "List Item Number:" + position);

                if (position < mGroupUsers.size()) {
                    Intent intent = new Intent(getApplicationContext(), ShoppingListActivity.class);
                    intent.putExtra("USER_ID", mGroupUsers.get(position).getId());
                    startActivity(intent);
                 } else {
                    Toast.makeText(getApplicationContext(), "Need to add new members", Toast.LENGTH_SHORT).show();
                    Intent intent1 = new Intent(getApplicationContext(),RegistrationActivity.class);
//                    intent1.putExtra("GROUP_ID",new Long(data.getLong("GROUP_ID")));
                    startActivity(intent1);
                }
            }
        });
    }


    private void  updateUserList(){
        mGroupUsers.clear();

        List<GroupRegistrations> lGroupData = GroupRegistrations.find(GroupRegistrations.class,
                "grahak_group=?", mGroupId.toString());

        for (GroupRegistrations i : lGroupData) {
            mGroupUsers.add(i.getContact());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_add_aadhaar) {
            Intent intent = new Intent(this, AdhaarDisplayActivity.class);
            startActivity(intent);
            //finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            return true;
        }
        if(id == R.id.action_plot_on_map){
            Intent intent = new Intent(this, OfflinePlotting.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        Log.d("debug", "OnResume called");

        updateUserList();
        mGroupMembersListAdapter.notifyDataSetChanged();

        super.onResume();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
