package in.geektronics.yahanpay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import in.geektronics.yahanpay.adapters.ProductListAdapter;
import in.geektronics.yahanpay.models.Product;
import in.geektronics.yahanpay.models.ProductCategory;
import in.geektronics.yahanpay.models.ProductOrder;
import in.geektronics.yahanpay.models.User;
import in.geektronics.yahanpay.utils.CSVFileParser;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class ShoppingListActivity extends ActionBarActivity {

    ProductListAdapter productsListAdapter;
    List <String> categoriesList;
    HashMap<String, List<Product>> categoryProductsMap;
    User user;

    @InjectView(R.id.listView_products)
    ExpandableListView productsListView;

    @OnClick(R.id.view_report)
    public void viewReport(){
        updateCart();

        Intent intent = new Intent(getApplicationContext(),UserReportActivity.class);
        intent.putExtra("USER_ID",user.getId());
        startActivity(intent);
    }

    private void updateCart(){
        for(ProductOrder lProdOrder : productsListAdapter.getProductOrder()){
            ProductOrder lDbOrder = Select.from(ProductOrder.class)
                    .where(Condition.prop("product").eq(lProdOrder.getProduct().getId()),
                            Condition.prop("user").eq(user.getId())).first();

            if (lDbOrder != null) {
                lDbOrder.setQuantity(lProdOrder.getQuantity());
                lDbOrder.save();
            } else {
                lProdOrder.save();
            }
        }
    }


    @OnClick(R.id.submit_button)
    public void submitData(){
        //Toast.makeText(getApplicationContext(),"Need to upload data",Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.default_font))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        setContentView(R.layout.activity_shopping_list);

        ButterKnife.inject(this);

        Intent intent = getIntent();

        user = User.findById(User.class,intent.getLongExtra("USER_ID", 0));

        categoryProductsMap = getData(getApplicationContext());
        categoriesList = new ArrayList<String>(categoryProductsMap.keySet());
        productsListAdapter = new ProductListAdapter(getApplicationContext(), categoriesList, categoryProductsMap);
        productsListAdapter.setUser(user);
        productsListView.setAdapter(productsListAdapter);

//        productsListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                Product product = categoryProductsMap.get(categoriesList.get(groupPosition)).get(childPosition);
//
//                ProductOrder lProductOrder = Select.from(ProductOrder.class)
//                        .where(Condition.prop("product").eq(product.getId()),
//                                Condition.prop("user").eq(user.getId())).first();
//
//                if(id != R.id.decrease_count) {
//                    if (lProductOrder != null) {
//                        ((TextView) v.findViewById(R.id.product_count)).setText("" + (lProductOrder.getQuantity() + 1));
//                        lProductOrder.setQuantity(lProductOrder.getQuantity() + 1);
//                        lProductOrder.save();
//                    } else {
//                        lProductOrder = new ProductOrder(user, product, 1);
//                        ((TextView) v.findViewById(R.id.product_count)).setText("" + lProductOrder.getQuantity());
//                        ((TextView) v.findViewById(R.id.product_count)).setVisibility(View.VISIBLE);
//                        ((TextView) v.findViewById(R.id.decrease_count)).setVisibility(View.VISIBLE);
//                        lProductOrder.save();
//                    }
//                }
//                else {
//                    Log.d("Debugging","Button Pressed");
//                    if (lProductOrder != null) {
//                        if(lProductOrder.getQuantity() != 0) {
//                            ((TextView) v.findViewById(R.id.product_count)).setText("" + (lProductOrder.getQuantity() - 1));
//                            lProductOrder.setQuantity(lProductOrder.getQuantity() - 1);
//                            lProductOrder.save();
//                        }
//                    }
//                }
//                return false;
//            }
//        });


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public HashMap<String,List<Product>> getData(Context context) {
        HashMap<String, List<Product>> productsDetail = new HashMap<String, List<Product>>();

        if(Product.find(Product.class,null,null).size() == 0) {
            List<String[]> productsData = CSVFileParser
                    .parseFileFromResource(context, R.raw.products);

            for (int i = 1; i < productsData.size(); i++) {
                // The add product to database class creates the product Catagory as well
                Product lProduct = new Product(productsData.get(i));
                lProduct.save();
            }
        }

        for (ProductCategory i : Select.from(ProductCategory.class).list()) {
            productsDetail.put(i.getProductCategory(), Select.from(Product.class).where(Condition.prop("category").eq(i.getId())).list());
        }

        return productsDetail;
    }
}
