package in.geektronics.yahanpay;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import in.geektronics.yahanpay.models.AadhaarCard;
import in.geektronics.yahanpay.network.WebService;
import in.geektronics.yahanpay.network.WebServiceListener;

public class MapActivity extends FragmentActivity implements WebServiceListener{
    // Google Map
    private GoogleMap googleMap;
    AadhaarCard card;
    WebService request;
    String address;
    String name;
    LatLng latLng = null ;
    public String TAG = "MAP_ACTIVITY" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        try {
            // Loading map
            initilizeMap();
            card = (AadhaarCard)getIntent().getParcelableExtra("card");
            
            Log.d(TAG, "card " + card.toString());
            
            if(card.lat.equals("0.0") && card.lng.equals("0.0")){
            	Log.d(TAG, "Calling WebService");
	            request = new WebService(this,this);
	    		if(card !=null ){
	    			request.execute(card);
	    		}
	    		else{
	    			Toast.makeText(getApplicationContext(),
                            "Sorry! problem with address", Toast.LENGTH_SHORT)
	                        .show();
	    		}
            }
            else{
				latLng = new LatLng(Double.parseDouble(card.lat), Double.parseDouble(card.lng));
				this.plotOfflineAddress(latLng);
            }
            googleMap.setMyLocationEnabled(true);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Exception");
        }
 
    }

    private void plotOfflineAddress(LatLng pos) {
    	Log.d(TAG, "in plotOfflineAddress");
    	 MarkerOptions marker = new MarkerOptions().position(pos).title(name);
         googleMap.addMarker(marker);
         
         CameraPosition cameraPosition = new CameraPosition.Builder()
         .target(pos).tilt(45).zoom(15).build();
		    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}

	//method to create map
    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(
                    R.id.map)).getMap();
 
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
 
    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }


	public void onNetworkActionComplete(AadhaarCard c) {
		if(!c.lat.equals("0") || !c.lng.equals("0")){
			card.lat = c.lat ;
			card.lng = c.lng ;
            card.save();
			LatLng latLng = new LatLng(Double.parseDouble(card.lat), Double.parseDouble(card.lng));
			Log.d(TAG, "Check card object : lat " + card.lat);
			Log.d(TAG, "Card deleted and new card stored with lat lng");
			Log.d(TAG, latLng.toString());

            ArrayList<AadhaarCard> cards = new ArrayList<>(AadhaarCard.find(AadhaarCard.class,null,null));

			for(AadhaarCard card : cards){
				Log.d(TAG, "verifying latlng " + card.name + " " + card.lat + " " + card.lng);
			}
            MarkerOptions marker = new MarkerOptions().position(latLng).title(name);
            googleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).tilt(45).zoom(15).build();
		    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
	}

	@Override
	public void onConnectionCheck(boolean b) {
		// TODO Auto-generated method stub
	}
}