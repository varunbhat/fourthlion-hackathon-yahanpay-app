package in.geektronics.yahanpay;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import in.geektronics.yahanpay.fragments.group_list.GroupListViewFragment;
import in.geektronics.yahanpay.fragments.group_list.NewGroupFragment;
import in.geektronics.yahanpay.models.GrahakGroup;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class GroupListActivity extends ActionBarActivity implements
        GroupListViewFragment.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.default_font))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        setContentView(R.layout.activity_group_list);

        //DEBUG
//        Iterator<GroupRegistrations> lUsers = GroupRegistrations.findAll(GroupRegistrations.class);
//        while(lUsers.hasNext()){
//            GroupRegistrations lReg = lUsers.next();
//            Log.d("CreateGroupDB", "Registered Users----- " + "Name:" +
//                    lReg.getContact().getName() + " Group Name:" +
//                    lReg.getGrahakGroup().getGroupName());
//        }
        // /DEBUG

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment lReplacementFragment = null;

        if(GrahakGroup.find(GrahakGroup.class, "is_admin=? or is_member=?", "1", "1").size()==0)
            lReplacementFragment = new NewGroupFragment();
        else
            lReplacementFragment = new GroupListViewFragment();

        Bundle data = new Bundle();
        fragmentTransaction.addToBackStack(null);
        lReplacementFragment.setArguments(data);
        fragmentTransaction.add(R.id.container, lReplacementFragment);
        fragmentTransaction.commit();

//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.main_actionbar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment lReplacementFragment = null;

        if(GrahakGroup.find(GrahakGroup.class,"is_admin=? or is_member=?", "1","1").size()==0)
            lReplacementFragment = new NewGroupFragment();
        else
            lReplacementFragment = new GroupListViewFragment();

        Bundle data = new Bundle();
        fragmentTransaction.addToBackStack(null);
        lReplacementFragment.setArguments(data);
        fragmentTransaction.replace(R.id.container, lReplacementFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction(String id) {
    }
}
